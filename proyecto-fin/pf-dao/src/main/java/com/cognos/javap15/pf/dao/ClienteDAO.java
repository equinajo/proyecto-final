/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.pf.dao;

import com.cognos.javap15.pf.model.Cliente;
import com.cognos.javap15.pf.model.Compra;
import com.cognos.javap15.pf.model.Producto;
import com.cognos.javap15.pf.model.Usuario;
import java.util.List;

/**
 *
 * @author JAVA
 */
public interface ClienteDAO {

    List<Cliente> listarClientes();
    
    Cliente registrarCliente(Cliente cliente);
    
    Cliente registrarCliente(Cliente cliente,List<Compra> listCompra);
    
    Cliente modificarCliente(Cliente cliente);
    
    Cliente findClienteByNombreByRfc(Cliente cliente);
    
    void eliminarCliente(Cliente cliente);
    
   
}
