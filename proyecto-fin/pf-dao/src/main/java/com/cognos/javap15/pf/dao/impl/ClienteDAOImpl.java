/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.pf.dao.impl;

import com.cognos.javap15.pf.dao.ClienteDAO;
import com.cognos.javap15.pf.model.Cliente;
import com.cognos.javap15.pf.model.Compra;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

/**
 *
 * @author JAVA
 */
@Stateless
public class ClienteDAOImpl implements ClienteDAO {

    @PersistenceContext(unitName = "proy-fin")
    EntityManager em;

    @Override
    public List<Cliente> listarClientes() {
        return em.createNamedQuery(Cliente.LISTAR_CLIENTES).getResultList();
    }

    @Override
    public Cliente registrarCliente(Cliente cliente) {
        cliente.trimUpperCase();
        em.persist(cliente);
        return cliente;
    }

    @Override
    public Cliente registrarCliente(Cliente cliente, List<Compra> listCompra) {

        if (cliente.getIdCliente() == null) {
            //Nuevo registro
            em.persist(cliente);
        } else {
            //Modificacion de registro
        }

        if (listCompra != null && !listCompra.isEmpty()) {

            StringBuilder sb = new StringBuilder("");
            sb.append(" SELECT c ");
            sb.append(" FROM Compra c ");
            sb.append(" WHERE c.idCliente = :idCliente");
            Query query = em.createQuery(sb.toString());
            query.setParameter("idCliente", cliente.getIdCliente());

            List<Compra> listCompraAux = query.getResultList();
            //Eliminando las compras que ya no estan registradas
            for (Compra compraAux : listCompraAux) {
                boolean existe = false;
                for (Compra compra : listCompra) {
                    if (compraAux.getIdProducto().getIdProducto().equals(compra.getIdProducto().getIdProducto())) {
                        existe = true;
                    }
                }
                if (!existe) {
                    em.remove(compraAux);
                }
            }
            //Actualizando nuevos registros
            for (Compra compra : listCompra) {
                if (compra.getIdCompra() == null) {
                    //Nuevo registro
                    Compra compraNew = new Compra();
                    compraNew.setIdCliente(cliente);
                    em.persist(compraNew);
                } else {
                    //Modificacion del registro

                }

            }
        }
        return cliente;
    }

    @Override
    @Transactional(rollbackOn = {Exception.class})
    public synchronized Cliente modificarCliente(Cliente cliente) {
        try {
            System.out.println("Obtener cliente " + cliente.getIdCliente());
            Cliente clienteDB = em.find(Cliente.class, cliente.getIdCliente());
            clienteDB.setNombre(cliente.getNombre());
            clienteDB.setApellido(cliente.getApellido());
            clienteDB.setDireccion(cliente.getDireccion());
            clienteDB.setFechaNacimiento(cliente.getFechaNacimiento());
            clienteDB.setRfc(cliente.getRfc());
            clienteDB.trimUpperCase();
            cliente = em.merge(clienteDB);
            return cliente;
        } catch (Exception ex) {
            Logger.getLogger(ClienteDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cliente;
    }

    @Override
    public Cliente findClienteByNombreByRfc(Cliente cliente) {
        StringBuilder sb = new StringBuilder("");
        sb.append(" SELECT c ");
        sb.append(" FROM Cliente c ");
        sb.append(" WHERE trim(upper(c.nombre)) = trim(upper(:nombre))");
        sb.append(" AND trim(upper(c.rfc)) = trim(upper(:rfc))");
        sb.append(" AND c.idCliente <> :idCliente");
        Query query = em.createQuery(sb.toString());
        query.setParameter("nombre", cliente.getNombre());
        query.setParameter("rfc", cliente.getRfc());
        query.setParameter("idCliente", cliente.getIdCliente());
        List<Cliente> listCliente = query.getResultList();

        if (listCliente != null && !listCliente.isEmpty()) {
            return listCliente.get(0);
        } else {
            return null;
        }
    }

    @Override
    public void eliminarCliente(Cliente cliente) {

        //Eliminando compras
        StringBuilder sb = new StringBuilder("");
        sb.append(" SELECT c ");
        sb.append(" FROM Compra c ");
        sb.append(" WHERE c.idCliente.idCliente = :idCliente");
        Query query = em.createQuery(sb.toString());
        query.setParameter("idCliente", cliente.getIdCliente());
        List<Compra> listCompra = query.getResultList();

        for (Compra compra : listCompra) {
            Compra compraBase = em.find(Compra.class, compra.getIdCompra());
            em.remove(em.merge(compraBase));
        }
        
        Cliente clienteBase = em.find(Cliente.class, cliente.getIdCliente());
        em.remove(em.merge(clienteBase));
    }
}
