/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.pf.dao.impl;

import com.cognos.javap15.pf.dao.ProductoDAO;
import com.cognos.javap15.pf.model.Cliente;
import com.cognos.javap15.pf.model.Compra;
import com.cognos.javap15.pf.model.Producto;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

/**
 *
 * @author JAVA
 */
@Stateless
public class ProductoDAOImpl implements ProductoDAO {

    @PersistenceContext(unitName = "proy-fin")
    EntityManager em;

    @Override
    public List<Producto> listarProductos() {
        return em.createNamedQuery(Producto.LISTAR_PRODUCTOS).getResultList();
    }

    @Override
    public List<Compra> listarComprasByIdCliente(Long idCliente) {
        StringBuilder sb = new StringBuilder("");
        sb.append(" SELECT c ");
        sb.append(" FROM Compra c ");
        sb.append(" WHERE c.idCliente.idCliente = :idCliente");
        Query query = em.createQuery(sb.toString());
        query.setParameter("idCliente", idCliente);
        List<Compra> listCompra = query.getResultList();
        return listCompra;
    }

    @Override
    public Producto registrarProducto(Producto producto) {
        producto.trimUpperCase();
        em.persist(producto);
        return producto;
    }

    @Override
    @Transactional(rollbackOn = {Exception.class})
    public synchronized Producto modificarProducto(Producto producto) {
        try {
            System.out.println("Obtener producto " + producto.getIdProducto());
            Producto productoDB = em.find(Producto.class, producto.getIdProducto());
            productoDB.setNombre(producto.getNombre());
            productoDB.setCodigo(producto.getCodigo());
            productoDB.setPrecioUnitario(producto.getPrecioUnitario());
            producto.trimUpperCase();
            producto = em.merge(productoDB);
            return producto;
        } catch (Exception ex) {
            Logger.getLogger(ProductoDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return producto;
    }

    @Override
    @Transactional(rollbackOn = {Exception.class})
    public void persistirCompra(String codProducto, Cliente cliente) {
        try {
            System.out.println("cliente.getIdCliente() "+cliente.getIdCliente());
            if (cliente.getIdCliente() != null) {
                StringBuilder sb = new StringBuilder("");
                sb.append(" SELECT c ");
                sb.append(" FROM Compra c ");
                sb.append(" WHERE c.idCliente.idCliente = :idCliente");
                Query query = em.createQuery(sb.toString());
                query.setParameter("idCliente", cliente.getIdCliente());
                List<Compra> listCompra = query.getResultList();

                boolean existe = false;
                for (Compra compra : listCompra) {
                    if (compra.getIdProducto().getIdProducto() == Long.parseLong(codProducto)) {
                        existe = true;
                    }
                }
                
                System.out.println("existe: " +existe);
                System.out.println("codProducto: " +codProducto);
                
                if (!existe) {
                    Producto productoNew = em.find(Producto.class, Long.parseLong(codProducto));
                    Compra compraAux = new Compra();
                    compraAux.setIdProducto(productoNew);
                    compraAux.setIdCliente(cliente);
                    em.merge(compraAux);
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(ProductoDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void eliminarCompra(Compra compra){
        System.out.println("compra.getIdCompra() "+compra.getIdCompra());
        Compra compraBase = em.find(Compra.class, compra.getIdCompra());
        em.remove(em.merge(compra));
    }
    
    @Override
    public void eliminarProducto(Producto producto){
        System.out.println("compra.getIdCompra() "+producto.getIdProducto());
        Producto productoBase = em.find(Producto.class, producto.getIdProducto());
        em.remove(em.merge(productoBase));
    }
}
