/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.pf.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author JAVA
 */
@Entity
@Table(name = "cliente")


@NamedQueries({
    @NamedQuery(name = Cliente.LISTAR_CLIENTES, query = "SELECT u FROM Cliente u")
})
public class Cliente implements Serializable {
    
    public final static String LISTAR_CLIENTES = "Cliente.ListarClientes";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)    
    @Column(name = "id_cliente")
    //@Version()
    private Long idCliente;
    
    @Basic
    @Column(name = "nombre")
    private String nombre;
    
    @Basic
    @Column(name = "apellido")
    private String apellido;
    
    @Basic
    @Column(name = "rfc")
    private String rfc;
    
    @Basic
    @Column(name = "direccion")
    private String direccion;
    
    @Basic
    @Column(name = "fecha_nacimiento")
    private String fechaNacimiento;
    

    public Cliente() {
    }

    public Cliente(Long idCliente, String nombre, String apellido, String rfc, String direccion, String fechaNacimiento) {
        this.idCliente = idCliente;
        this.nombre = nombre;
        this.apellido = apellido;
        this.rfc = rfc;
        this.direccion = direccion;
        this.fechaNacimiento = fechaNacimiento;
    }

    public void trimUpperCase(){
        this.nombre=nombre.trim().toUpperCase();
        if(apellido!=null)
        this.apellido=apellido.trim().toUpperCase();
        this.rfc=rfc.trim().toUpperCase();
        if(direccion!=null)
        this.direccion=direccion.trim().toUpperCase();
    }
    
    public Long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Long idCliente) {
        this.idCliente = idCliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
}
