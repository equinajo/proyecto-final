/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.pf.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author JAVA
 */
@Entity
@Table(name = "producto")

@NamedQueries({
    @NamedQuery(name = Producto.LISTAR_PRODUCTOS, query = "SELECT u FROM Producto u")
})

public class Producto implements Serializable {

    public final static String LISTAR_PRODUCTOS = "Producto.ListarProductos";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) 
    @Column(name = "id_producto")
    private Long idProducto;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "codigo")
    private String codigo;

    @Column(name = "precio_unitorio")
    private String precioUnitario;

    public Producto() {
    }

    public void trimUpperCase() {
        this.nombre = nombre.trim().toUpperCase();
        this.codigo = codigo.trim().toUpperCase();
    }

    public Producto(Long idProducto, String nombre, String codigo, String presioUnitario) {
        this.idProducto = idProducto;
        this.nombre = nombre;
        this.codigo = codigo;
        this.precioUnitario = presioUnitario;
    }

    public Long getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Long idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(String presioUnitario) {
        this.precioUnitario = presioUnitario;
    }
}
