/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.pf.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 *
 * @author JAVA
 */
@Entity
@Table(name = "usuario")


@NamedQueries({
    @NamedQuery(name = Usuario.LISTAR_USUARIOS, query = "SELECT u FROM Usuario u")
})
public class Usuario implements Serializable {
    
    public final static String LISTAR_USUARIOS = "Usuario.ListarUsuarios";
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)    
    @Column(name = "usuario_id")
    //@Version()
    private Long id;
    
    @Basic
    @Column(name = "login")
    private String login;
    
    @Basic
    @Column(name = "nombre")
    private String nombre;
    
    @Basic
    @Column(name = "apellido")
    private String apellido;
    
    @Basic
    @Column(name = "email")
    private String email;
    
    @Basic
    @Column(name = "password")
    private String password;
    
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "usuario_rol", joinColumns = {@JoinColumn(name = "usuario_id")}, 
            inverseJoinColumns = {@JoinColumn(name = "cod_rol")})
    private List<Rol> roles;

    public Usuario() {
    }

    public Usuario(Long id, String login, String nombre, String apellido, String email) {
        this.id = id;
        this.login = login;
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Rol> getRoles() {
        return roles;
    }

    public void setRoles(List<Rol> roles) {
        this.roles = roles;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
    
}
