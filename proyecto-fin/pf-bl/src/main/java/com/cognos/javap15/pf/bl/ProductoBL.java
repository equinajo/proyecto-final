/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.pf.bl;

import com.cognos.javap15.pf.model.Cliente;
import com.cognos.javap15.pf.model.Compra;
import com.cognos.javap15.pf.model.Producto;
import java.util.List;

/**
 *
 * @author JAVA
 */
public interface ProductoBL {
    
    List<Producto> listarProductos();
    
    List<Compra> listarComprasByIdCliente(Long idCliente);
    
    Producto registrarProducto(Producto producto);
    
    Producto modificarProducto(Producto producto);
    
    void persistirCompra(String codProducto,Cliente cliente);
    
    void eliminarCompra(Compra compra);
    
    void eliminarProducto(Producto producto);
}
