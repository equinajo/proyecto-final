/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.pf.bl.impl;

import com.cognos.javap15.pf.bl.ClienteBL;
import com.cognos.javap15.pf.bl.ProductoBL;
import com.cognos.javap15.pf.bl.UsuarioBL;
import com.cognos.javap15.pf.dao.ClienteDAO;
import com.cognos.javap15.pf.dao.ProductoDAO;
import com.cognos.javap15.pf.model.Usuario;
import com.cognos.javap15.pf.dao.UsuarioDAO;
import com.cognos.javap15.pf.model.Cliente;
import com.cognos.javap15.pf.model.Compra;
import com.cognos.javap15.pf.model.Producto;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author JAVA
 */
@Stateless
public class ProductoBLImpl implements ProductoBL{

    @Inject
    private ProductoDAO productoDAO;
    
    @Override
    public List<Producto> listarProductos() {
        return productoDAO.listarProductos();
    }
    
    @Override
    public List<Compra> listarComprasByIdCliente(Long idCliente) {
        return productoDAO.listarComprasByIdCliente(idCliente);
    }

    @Override
    public Producto registrarProducto(Producto producto) {
        return productoDAO.registrarProducto(producto);
    }

    @Override
    public Producto modificarProducto(Producto producto) {
        return productoDAO.modificarProducto(producto);
    }
    
    @Override
    public void persistirCompra(String codProducto,Cliente cliente){
        System.out.println("SDADADSD");
        productoDAO.persistirCompra(codProducto,cliente);
    }
    
    @Override
    public void eliminarCompra(Compra compra){
        productoDAO.eliminarCompra(compra);
    }
    
    @Override
    public void eliminarProducto(Producto producto){
        productoDAO.eliminarProducto(producto);
    }
}
