/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.pf.bl.impl;

import com.cognos.javap15.pf.bl.ClienteBL;
import com.cognos.javap15.pf.dao.ClienteDAO;
import com.cognos.javap15.pf.model.Cliente;
import com.cognos.javap15.pf.model.Compra;
import com.cognos.javap15.pf.model.Producto;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

/**
 *
 * @author JAVA
 */
@Stateless
public class ClienteBLImpl implements ClienteBL{

    @Inject
    private ClienteDAO clienteDAO;
    
    @Override
    public List<Cliente> listarClientes() {
        return clienteDAO.listarClientes();
    }

    @Override
    public Cliente registrarCliente(Cliente cliente) {
        return clienteDAO.registrarCliente(cliente);
    }
    
    @Override
    public Cliente registrarCliente(Cliente cliente,List<Compra> listCompra) {
        return clienteDAO.registrarCliente(cliente,listCompra);
    }

    @Override
    public Cliente modificarCliente(Cliente cliente) {
        return clienteDAO.modificarCliente(cliente);
    }
    
    @Override
    public Cliente findClienteByNombreByRfc(Cliente cliente){
        return clienteDAO.findClienteByNombreByRfc(cliente);
    }
    
    @Override
    public void eliminarCliente(Cliente cliente){
        clienteDAO.eliminarCliente(cliente);
    }
}
