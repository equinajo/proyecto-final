/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.pf.jsf.beans;

import com.cognos.javap15.pf.bl.ProductoBL;
import com.cognos.javap15.pf.bl.UsuarioBL;
import com.cognos.javap15.pf.model.Compra;
import com.cognos.javap15.pf.model.Producto;
import com.cognos.javap15.pf.model.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author JAVA
 */
@Named
@RequestScoped
public class ProductoBean implements Serializable {

    @Inject
    private ProductoBL productoBL;

    private List<Producto> listProducto;
    private Producto producto;
    private String errorProducto;

    @PostConstruct
    public void init() {
        actualizarUsuarios();
        producto = new Producto();
    }

    public void actualizarUsuarios() {
        listProducto = productoBL.listarProductos();
        errorProducto = "";
    }

    public void registrarProducto() {
        System.out.println("nuevo");
        if(producto.getNombre().equals("") || producto.getNombre()==null){
            errorProducto = "Debe completar el campo Nombre";
            return;
        }
        if(producto.getCodigo().equals("") || producto.getCodigo()==null){
            errorProducto = "Debe completar el campo Codigo";
            return;
        }
        
        if(producto.getPrecioUnitario().equals("") || producto.getPrecioUnitario()==null){
            errorProducto = "Debe completar el campo Precio Unitario";
            return;
        }
        if(producto.getIdProducto()==null){
         productoBL.registrarProducto(producto);   
        }else{
            productoBL.modificarProducto(producto);
        }
        producto = new Producto();
        actualizarUsuarios();
    }

    public void cancelar(){
        actualizarUsuarios();
        producto = new Producto();
    }
    
    public void modificarProducto() {
        productoBL.modificarProducto(producto);
        producto = new Producto();
        actualizarUsuarios();
    }

    public void seleccionarProducto(Producto selectProducto) {
        System.out.println("---" + selectProducto.getIdProducto());
        this.producto = selectProducto;
    }

    public void eliminarProducto(Producto selectProducto) {

        List<Compra> listCompra = productoBL.listarComprasByIdCliente(selectProducto.getIdProducto());

        if (listCompra == null || listCompra.isEmpty()) {
            productoBL.eliminarProducto(selectProducto);
            actualizarUsuarios();
        }else{
            errorProducto = "No se puede eliminar el producto";
        }

    }

    public List<Producto> getListProducto() {
        return listProducto;
    }

    public void setListProducto(List<Producto> listProducto) {
        this.listProducto = listProducto;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public String getErrorProducto() {
        return errorProducto;
    }

    public void setErrorProducto(String errorProducto) {
        this.errorProducto = errorProducto;
    }

}
