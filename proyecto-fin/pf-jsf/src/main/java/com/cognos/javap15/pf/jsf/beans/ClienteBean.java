/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.pf.jsf.beans;

import com.cognos.javap15.pf.bl.ClienteBL;
import com.cognos.javap15.pf.bl.ProductoBL;
import com.cognos.javap15.pf.model.Cliente;
import com.cognos.javap15.pf.model.Compra;
import com.cognos.javap15.pf.model.Producto;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author JAVA
 */
@Named
@ViewScoped
public class ClienteBean implements Serializable {

    @Inject
    private ClienteBL clienteBL;
    @Inject
    private ProductoBL productoBL;

    private List<Cliente> listCliente;
    private Cliente cliente;
    private List<Compra> listCompra;
    private List<Producto> listProductoAux;
    private List<SelectItem> selectListProducto;
    private Producto producto;
    private String codProducto;
    private String errorCompras;
    private String errorCliente;

    @PostConstruct
    public void init() {
        cargar();
    }

    public void cargar() {
        //Inicializando datos del cliente
        cliente = new Cliente();
        //Inicializando datos del producto
        codProducto = "";
        listCompra = new ArrayList<>();
        listProductoAux = productoBL.listarProductos();
        selectListProducto = new ArrayList<>();
        if (listProductoAux != null) {
            for (Producto prod : listProductoAux) {
                selectListProducto.add(new SelectItem(prod.getIdProducto(), prod.getNombre()));
            }
        }
        //Inicializando datos del listado de clientes
        listCliente = new ArrayList<>();
        listCliente = clienteBL.listarClientes();
        errorCliente = "";
        errorCompras = "";
    }

    public void guardarCliente() {
        System.out.println("guardarCliente");
        if (cliente.getNombre() == null || cliente.getNombre().equals("")) {
            errorCliente = "Debe completar el campo Nombre";
            return;
        }
        if (cliente.getRfc() == null || cliente.getRfc().equals("")) {
            errorCliente = "Debe completar el campo RFC";
            return;
        }
        Cliente clienteBase = clienteBL.findClienteByNombreByRfc(cliente);
        errorCliente = "";
        if (clienteBase != null) {
            errorCliente = "Ya se encuentra registrado un cliente con los mismos datos";
            return;
        }
        if (cliente.getIdCliente() == null) {
            clienteBL.registrarCliente(cliente);
        } else {
            clienteBL.modificarCliente(cliente);
        }
        cargar();
    }

    public void cancelar() {
        cargar();
    }

    public void guardarProducto() {
        System.out.println("guardarProducto");
        System.out.println("cliente.getIdCliente() " + cliente.getIdCliente());
        errorCompras = "";
        if (cliente.getIdCliente() == null || cliente.getIdCliente() == 0) {
            errorCompras = "Debe seleccionar un cliente";
            return;
        }
        if (codProducto == null || codProducto.equals("")) {
            errorCompras = "Debe seleccionar un producto";
            return;
        }
        productoBL.persistirCompra(codProducto, cliente);
        listCompra = productoBL.listarComprasByIdCliente(cliente.getIdCliente());
        codProducto = "";
    }

    public void selectModificarCliente(Cliente selectCliente) {
        this.cliente = selectCliente;
        listCompra = productoBL.listarComprasByIdCliente(cliente.getIdCliente());
    }

    public void eliminarCliente(Cliente selectCliente) {
        clienteBL.eliminarCliente(selectCliente);
        cargar();
    }

    public void eliminarProducto(Compra selectCompra) {
        System.out.println("sdsadasd");
        System.out.println("selectCompra " + selectCompra);
        productoBL.eliminarCompra(selectCompra);
        listCompra = productoBL.listarComprasByIdCliente(cliente.getIdCliente());
    }

    public void modificarUsuario() {
        /*System.out.println("editar" + usuarioNuevo.getId());
        usuarioBL.modificarUsuario(usuarioNuevo);
        usuarioNuevo = new Usuario();
        //actualizarUsuarios();*/
    }

    public List<Producto> getListProductoAux() {
        return listProductoAux;
    }

    public void setListProductoAux(List<Producto> listProductoAux) {
        this.listProductoAux = listProductoAux;
    }

    public List<SelectItem> getSelectListProducto() {
        return selectListProducto;
    }

    public void setSelectListProducto(List<SelectItem> selectListProducto) {
        this.selectListProducto = selectListProducto;
    }

    public String getCodProducto() {
        return codProducto;
    }

    public void setCodProducto(String codProducto) {
        this.codProducto = codProducto;
    }

    public List<Cliente> getListCliente() {
        return listCliente;
    }

    public void setListCliente(List<Cliente> listCliente) {
        this.listCliente = listCliente;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<Compra> getListCompra() {
        return listCompra;
    }

    public void setListCompra(List<Compra> listCompra) {
        this.listCompra = listCompra;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public String getErrorCompras() {
        return errorCompras;
    }

    public void setErrorCompras(String errorCompras) {
        this.errorCompras = errorCompras;
    }

    public String getErrorCliente() {
        return errorCliente;
    }

    public void setErrorCliente(String errorCliente) {
        this.errorCliente = errorCliente;
    }

}
