/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cognos.javap15.pf.jsf.beans;

import com.cognos.javap15.pf.bl.UsuarioBL;
import com.cognos.javap15.pf.model.Usuario;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author JAVA
 */
@Named
@RequestScoped
public class UsuarioBean implements Serializable{

    @Inject
    private UsuarioBL usuarioBL;

    private List<Usuario> usuarios;
    private Usuario usuarioNuevo;

    @PostConstruct
    public void init() {
        actualizarUsuarios();
        usuarioNuevo = new Usuario();
    }

    public void actualizarUsuarios() {
        usuarios = usuarioBL.listarUsuarios();
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public Usuario getUsuarioNuevo() {
        return usuarioNuevo;
    }

    public void setUsuarioNuevo(Usuario usuarioNuevo) {
        this.usuarioNuevo = usuarioNuevo;
    }

    public void registrarUsuario() {
        System.out.println("nuevo");
        usuarioBL.registrarUsuario(usuarioNuevo);
        usuarioNuevo = new Usuario();
        actualizarUsuarios();
    }

    public void modificarUsuario() {
        System.out.println("editar" + usuarioNuevo.getId());
        usuarioBL.modificarUsuario(usuarioNuevo);
        usuarioNuevo = new Usuario();
        actualizarUsuarios();
    }

    public void seleccionarUsuario(Usuario usuario) {
        System.out.println("---" + usuario.getId());
        this.usuarioNuevo = usuario;

    }

}
